from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    title = 'Home'

    return render_template('pages/index.html', title=title)

@app.route('/contact')
def contact():
    title = 'Contact us'

    return render_template('pages/contact.html', title=title)

@app.route('/about')
def about():
    title = 'About'

    return render_template('pages/about.html', title=title)

@app.route('/buildguide')
def buildguide():
    title = 'Build guide'

    return render_template('pages/buildguide.html', title=title)

if __name__ == '__main__':
    app.run(debug=True)
